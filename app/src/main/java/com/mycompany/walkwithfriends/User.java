package com.mycompany.walkwithfriends;

import android.content.Context;

/**
 * Created by Chris on 4/13/2015.
 */
public class User {
    StepCount currentStepCount;
    StepHistory history;
    //GoalList goals;
    //AchievementList achievements;
    //Settings settings;
    //FriendList friends;
    String username;

    public User(Context c){
        currentStepCount = new StepCount(c);
        history = new StepHistory();
        username = "Anonymous";
    }
}
