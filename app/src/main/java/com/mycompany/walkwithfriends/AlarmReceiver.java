package com.mycompany.walkwithfriends;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Chris on 4/23/2015.
 */
public class AlarmReceiver extends BroadcastReceiver{

    public void onReceive(Context c, Intent i){
        Calendar cal = new GregorianCalendar();
        MainActivity.updateGoogle(c);
        Toast.makeText(c, "Just updated Google", Toast.LENGTH_LONG).show();
        if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0){
            MainActivity.user.history.storeStepsForDay(MainActivity.user.currentStepCount, c);
            MainActivity.user.currentStepCount.resetForNewDay(c);
        }
        /*else{
            Toast.makeText(c, "Sorry", Toast.LENGTH_LONG).show();
        }*/
    }
}
