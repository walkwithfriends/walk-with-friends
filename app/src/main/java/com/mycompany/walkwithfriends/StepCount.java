package com.mycompany.walkwithfriends;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Chris on 4/13/2015.
 */
public class StepCount {
    Date date;
    int currentSteps;



    public StepCount (Context c){
        DateFormat dateFormatter;
        dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US);
        Date today = new Date();
        try {
            date = dateFormatter.parse(dateFormatter.format(today));
        }
        catch(Exception e){
            e.printStackTrace();
        }
            //determine whether application has already been started today. If yes, then string
            //stored in "LAST KILLED ON" will match date.toString (today's date) and we can load the
            //value that was stored when the app stopped most recently. Otherwise, we start from 0.
            SharedPreferences sharedPrefs = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
            if(sharedPrefs.getString("LAST KILLED ON", "").equals(date.toString())){
                currentSteps = 0;                   //sharedPrefs.getInt("Today's Steps", 0);
                sharedPrefs.edit().putBoolean("First Step", false).commit();
            }
            else{
                currentSteps = 0;
                sharedPrefs.edit().putInt("Today's Steps", 0).putBoolean("First Step", false).commit();
            }
    }

    public void resetForNewDay(Context c){
        DateFormat dateFormatter;
        dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US);
        Date today = new Date();
        try {
            date = dateFormatter.parse(dateFormatter.format(today));
        }
        catch(Exception e){
            e.printStackTrace();
        }
        currentSteps = 0;
        SharedPreferences sharedPrefs = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt("Today's Steps", 0);
        editor.putBoolean("First Step", false);
        editor.commit();
    }

    public void setSteps(int steps){
        this.currentSteps = steps;
        MainActivity.updateView();
    }

    public Integer getSteps(){
        return currentSteps;
    }

    public void endOfDay(){
        //TODO: set rank, add to StepHistory
    }

    /*TODO: public void endOfHour(int ) {

    }*/

    public void setDate(Date date){this.date = date;}
}
