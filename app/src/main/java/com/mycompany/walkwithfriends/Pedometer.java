package com.mycompany.walkwithfriends;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.*;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Chris on 4/14/2015.
 */
public class Pedometer implements SensorEventListener{
    private SensorManager sensorManager;
    SharedPreferences sharedPrefs;
    //boolean firstStepTaken = false;
    private int initialCount;

    public Pedometer(Context c){
        this.sensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
        this.sharedPrefs = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
    }

    protected void initializePedometer() {
        //super.onResume();
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        //this if statement runs only once to record the initial hardware value ( to be compared to)


        if (!sharedPrefs.getBoolean("First Step", false)){
            initialCount = (int) event.values[0];
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putInt("Initial Count", initialCount);   //store initial value so we can compute difference later...
            editor.putBoolean("First Step", true).commit(); //(this is needed when app continues from one day to next)
            //firstStepTaken = true;
        }
            //TODO: make this more robust (might have just done this successfully, need to test though...)
            MainActivity.user.currentStepCount.setSteps((int) (event.values[0] - sharedPrefs.getInt("Initial Count", 0)));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
