package com.mycompany.walkwithfriends;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by Chris on 4/14/2015.
 */
public class StepHistory {
    //ArrayList<StepCount> history;
    Calendar day0 = new GregorianCalendar();

    public static final long MILLIS_IN_DAY = 86400000;


    public static final long MILLISECONDS_IN_A_DAY = 86400000;

    //This method does not store a persistent value (and is therefore useless...)
    public StepHistory (){
        Date today;
        DateFormat dateFormatter;
        dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US);
        today = new Date();
        try {
            day0.setTime(dateFormatter.parse(dateFormatter.format(today)));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    //This method is only used for testing
    public void setDay0(Date date){
        this.day0.setTime(date);
    }

    //This method is a monster, but it works!
    public int getTotalStepsForRange(Date first, Date last, Context c){
        Calendar firstDay = new GregorianCalendar();
        firstDay.setTime(first);
        Calendar lastDay = new GregorianCalendar();
        lastDay.setTime(last);

        try {
            if (firstDay.before(day0)) {
                throw new Exception();
                //return -1;
            }
            if (lastDay.after(new GregorianCalendar())) {
                throw new Exception();
                // -2;
            }

            //int index = (int) getDifferenceInDays(day0, firstDay);
            int total = 0;
            int daysInRange = (int) getDifferenceInDays(firstDay, lastDay) + 1;
            for (int i = 0; i < daysInRange; i++) {
                total = total + getStepsForDay(firstDay,c);
                firstDay.setTimeInMillis(firstDay.getTimeInMillis() + MILLIS_IN_DAY);
                //index ++;
            }
            return total;
        }
        catch (Exception e){
            e.printStackTrace();
            assert false;
            return -1;
        }

    }

    //This method needs a clean (no time component) Calendar object ( I think)
    public int getStepsForDay(Calendar day, Context c) {
        SharedPreferences history = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        int steps =  history.getInt(day.getTime().toString(), 0);
        return steps;
    }

    public static void storeStepsForDay(StepCount stepsForToday, Context c){
        SharedPreferences persistentHistory = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = persistentHistory.edit();
        editor.putInt(stepsForToday.date.toString(), stepsForToday.getSteps());
        editor.commit();
        //Toast.makeText(c, "It stored the value " + stepsForToday.getSteps(), Toast.LENGTH_LONG).show();
    }

    public void saveDailySteps(StepCount current, Context c){
        SharedPreferences persistentHistory = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = persistentHistory.edit();
        editor.putInt("Today's Steps", (loadDailySteps(c) + current.getSteps())).commit();
    }

    public int loadDailySteps(Context c){
        SharedPreferences persistentHistory = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        return persistentHistory.getInt("Today's Steps", 0);
    }

    protected int loadLifetimeSteps(Context c) {
        SharedPreferences persistentHistory = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        int steps = persistentHistory.getInt("Lifetime Steps", 0);
        return steps;
    }

    protected void storeLifetimeStepCount(int lifetimeSteps, Context c){
        SharedPreferences persistentHistory = c.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = persistentHistory.edit();
        editor.putInt("Lifetime Steps", lifetimeSteps);
        editor.commit();
    }

    private long getDifferenceInDays(Calendar first, Calendar last) {
        long lastDay_millis = last.getTimeInMillis();
        long firstDay_millis = first.getTimeInMillis();
        long index = (lastDay_millis - firstDay_millis)/(MILLIS_IN_DAY);
        return index;
    }

    //used to test getTotalStepsForRange
    /*public String testHistory(Context c){
        //first we'll create some stepCounts
        StepCount sc1 = new StepCount();
        StepCount sc2 = new StepCount();
        StepCount sc3 = new StepCount();
        StepCount sc4 = new StepCount();

        //now we'll initialize their dates
        Date now = new Date();
        DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.US);
        try { now = dateFormatter.parse(dateFormatter.format(now)); }
        catch (Exception e) { e.printStackTrace();}
        Calendar today = new GregorianCalendar();
        today.setTime(now);

        sc4.setDate(today.getTime());
        today.setTimeInMillis(today.getTimeInMillis() - MILLIS_IN_DAY);
        sc3.setDate(today.getTime());
        today.setTimeInMillis(today.getTimeInMillis() - MILLIS_IN_DAY);
        sc2.setDate(today.getTime());
        today.setTimeInMillis(today.getTimeInMillis() - MILLIS_IN_DAY);
        sc1.setDate(today.getTime());
        today.setTimeInMillis(today.getTimeInMillis() - MILLIS_IN_DAY);
        setDay0(today.getTime());       //this way day0 is BEFORE all the other days

        //next we'll give them each a different number of steps
        sc1.setSteps(1000);
        sc2.setSteps(2000);
        sc3.setSteps(3000);
        sc4.setSteps(4000);

        //and store them
        storeStepsForDay(sc1, c);
        storeStepsForDay(sc2, c);
        storeStepsForDay(sc3, c);
        storeStepsForDay(sc4, c);

        //now for some calculations
        int test1 = getTotalStepsForRange(sc1.date, sc3.date, c);
        int test2 = getTotalStepsForRange(sc1.date, sc4.date, c);
        int test3 = getTotalStepsForRange(sc2.date, sc3.date, c);
        int test4 = getTotalStepsForRange(sc2.date, sc4.date, c);
        int test5 = getTotalStepsForRange(sc3.date, sc4.date, c);


        return "Results are: \n for test 1: " + test1 +"\n for test 2: " + test2 +"\n for test 3: " + test3 +
                "\n for test 4: " + test4 + "for test 5: " + test5 + "\n results should be: "
                + "6000, 10000, 5000, 9000, 7000";
    }*/

}
