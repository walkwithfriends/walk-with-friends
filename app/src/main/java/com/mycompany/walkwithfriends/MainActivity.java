package com.mycompany.walkwithfriends;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.google.example.games.basegameutils.GameHelper;

import java.util.Calendar;
import java.util.Random;


public class MainActivity extends BaseGameActivity implements View.OnClickListener {

    public static final String PREFS_NAME = "MyPrefsFile";

    static TextView steps;
    static TextView miles;
    static TextView calories;
    static TextView lifetimeSteps;
    static int weight = 66;
    static int height = 180;
    static TextView quote;
    Pedometer ped;
    static User user;
    private PendingIntent alarmIntent;
    static int savedLifetimeSteps = 0;
    static int savedDailySteps = 0;
    static GameHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ped = new Pedometer(this);
        ped.initializePedometer();
        user = new User(this);
        steps = (TextView) findViewById(R.id.count);
        miles = (TextView) findViewById(R.id.num_miles);
        calories = (TextView) findViewById(R.id.num_calories);
        quote = (TextView) findViewById(R.id.quote);
        lifetimeSteps = (TextView) findViewById(R.id.count_all_time);
        helper = getGameHelper();
        savedLifetimeSteps = user.history.loadLifetimeSteps(this);
        savedDailySteps = user.history.loadDailySteps(this);
        setEndOfDayAlarm();

        //start google sign in/leaderboards
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.show_leaderboard).setOnClickListener(this);
        findViewById(R.id.show_achievements).setOnClickListener(this);

        LinearLayout cal = (LinearLayout) findViewById(R.id.layout_miles_cals);
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog();
            }
        });
        setQuote();
    }

    private void showInputDialog() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText w = (EditText) promptView.findViewById(R.id.in_weight);
        final EditText f = (EditText) promptView.findViewById(R.id.in_feet);
        final EditText i = (EditText) promptView.findViewById(R.id.in_inches);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (w.getText().toString().trim().length() > 0) {
                            weight = Integer.parseInt(w.getText().toString());
                        } else if (f.getText().toString().trim().length() > 0 && i.getText().toString().trim().length() > 0) {
                            height = Integer.parseInt(f.getText().toString()) * 12
                                    + Integer.parseInt(i.getText().toString());
                        }
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(w.getWindowToken(), 0);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(w.getWindowToken(), 0);
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        w.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        alert.show();
    }

    public void test(View view){
        if (updateGoogle(this))
            Toast.makeText(this, "Update Successful", Toast.LENGTH_LONG).show();
    }

    //TODO: this hasn't been tested yet
    public static Boolean updateGoogle(Context c){
        if (helper.getApiClient().isConnected()) {
            Games.Leaderboards.submitScore(helper.getApiClient(),
                    c.getString(R.string.steps_leaderboard),
                    (user.currentStepCount.getSteps() + savedDailySteps)
            );
            updateAchievements(c);
            return true;
        }
        else return false;
    }

    public static void updateAchievements(Context c){
        //TODO: define the steps taken toward each achievement and increment as appropriate
        if((user.currentStepCount.getSteps() + savedLifetimeSteps) >= 10) {
            if(helper.getApiClient().isConnected())
            {
                Games.Achievements.unlock(helper.getApiClient(), c.getString(R.string.achievement_baby_steps));
            }
        }
    }

    public static long getRank(Context c) {
        LeaderboardScore score = Games.Leaderboards.loadCurrentPlayerLeaderboardScore(helper.getApiClient(),
                c.getString(R.string.steps_leaderboard),
                LeaderboardVariant.TIME_SPAN_DAILY,
                LeaderboardVariant.COLLECTION_PUBLIC)
                .await().getScore();
        Toast.makeText(c, score.getDisplayRank(), Toast.LENGTH_LONG).show();
        //Toast.makeText(c, buffer.get(1).getDisplayRank(), Toast.LENGTH_LONG).show();
        //Toast.makeText(c, buffer.get(2).getDisplayRank(), Toast.LENGTH_LONG).show();
        return score.getRank();
    }

    public void runTestGame(View view){

        if (getApiClient().isConnected()) {
            Games.Leaderboards.submitScore(getApiClient(),
                    getString(R.string.steps_leaderboard),
                    100);
        }

        if(getApiClient().isConnected())
            Games.Achievements.unlock(getApiClient(),
                    getString(R.string.achievement_test_achievement));

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sign_in_button) {
            beginUserInitiatedSignIn();
        } else if(view.getId() == R.id.show_leaderboard){
            startActivityForResult(Games.Leaderboards.getLeaderboardIntent(
                            getApiClient(), getString(R.string.steps_leaderboard)),
                    1);
        } else if(view.getId() == R.id.show_achievements) {
            startActivityForResult(Games.Achievements.getAchievementsIntent(
                            getApiClient()),
                    2);
        }
    }

    public void onSignInSucceeded() {
        findViewById(R.id.layout_signin).setVisibility(View.GONE);
        //findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
        findViewById(R.id.leaderboard_achievements).setVisibility(View.VISIBLE);
    }

    @Override
    public void onSignInFailed() {
        findViewById(R.id.layout_signin).setVisibility(View.VISIBLE);
        //findViewById(R.id.sign_out_button).setVisibility(View.GONE);
        findViewById(R.id.leaderboard_achievements).setVisibility(View.GONE);
    }

    private void setEndOfDayAlarm() {
        Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);
    }

    private void storeLifetimeStepCount(int lifetimeSteps){
        SharedPreferences persistentHistory = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = persistentHistory.edit();
        editor.putInt("Lifetime Steps", lifetimeSteps);
        editor.commit();
    }

    public void clearLifetimeStepCount(View view){
        storeLifetimeStepCount(0);
    }

    @Override
    protected void onStop(){
        storeLifetimeStepCount(user.currentStepCount.getSteps() + user.history.loadLifetimeSteps(this));
        user.history.saveDailySteps(user.currentStepCount, this);
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        SharedPreferences sharedPrefs = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("LAST KILLED ON", user.currentStepCount.date.toString());
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static void setQuote() {
        String[] quoth = new String[] {"All truly great thoughts are conceived while walking.", "Patience and wisdom walk hand in hand, like two one-armed lovers.", "Home is everything you can walk to.", "Walking . . . is how the body measures itself against the earth.", "When real people fall down in life, they get right back up and keep on walking.", "Walking with a friend in the dark is better than walking alone in the light.", "Walking is the best possible exercise. Habituate yourself to walk very far.", "In every walk with nature one receives far more than he seeks.", "An early-morning walk is a blessing for the whole day.", "They say, to walk, all you need is two feet. But really, all you need is hope."};
        int randomIndex = new Random().nextInt(10);
        quote.setText("\"" + quoth[randomIndex] + "\"");
    }

    private static void setMilesAndCalories() {
        int steps = user.currentStepCount.getSteps() + savedDailySteps;
        // ((feet in mile) / (height)) * magic number to approximate stride length
        double stepsInMile = (5280/(height/12))*.42;
        double numMiles = steps / stepsInMile;
        double roundedMiles = Math.round(numMiles * 100.0) / 100.0;
        miles.setText("" + roundedMiles);

        // multiply weight by magical number to approximate calories burned walking one mile
        double numCalories = weight * 0.57 * numMiles;
        double roundedCalories = Math.round(numCalories * 100.0) / 100.0;
        calories.setText("" + roundedCalories);
    }

    public static void updateView() {
        lifetimeSteps.setText("" + (savedLifetimeSteps + user.currentStepCount.getSteps()) + " steps all time");
        steps.setText("" + (savedDailySteps + user.currentStepCount.getSteps()));
        setMilesAndCalories();
    }
}